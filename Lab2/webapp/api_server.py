#this is the webserver API interface for our GPIO driver

from flask import Flask, request
import socket

#Get my machine name
if socket.gethostname().find('.') >= 0:
    hostname = socket.gethostname()
else:
    hostname = socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)
#add all the API routes here

#default routes
#curl 0.0.0.0:5000
@app.route("/")
def hello():
    return "Hello from API Server: Default request from (hostname):" + hostname + "!\n"

#GET request
@app.route("/getHello")
def getHello():
    return "Hello from API Server: Get Request !\n"

#post request
@app.route("/createHello", methods= ['POST'])
def postRequestHello():
    mydata = request.data
    print "Data:" + mydata
    assert request.path == '/createHello'
    assert request.method == 'POST'
    data = str(request.form['mykey'])
    #import pdb; pdb.set_trace()
    return "Hello API Server: You sent a " + request.method + \
        "message to route path" + request.path + \
        "\n\tData: " + data + "\n"


#add all the API routes here


## Run the website and make sure to make
##  it externally visible with 0.0.0.0:5000 (default)


if __name__ == "__main__":
    app.debug = True
    app.run(host = '0.0.0.0')
