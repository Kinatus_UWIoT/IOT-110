#this driver is the hardware interface for activating and reading RPI GPIO pins
#Gaurav Garg - hand coding Jan 20

import RPi.GPIO as GPIO

# lets define the pin configuration for 3 LEDs and 1 switch
LED1_PIN    = 18    #connected to Red LED
LED2_PIN    = 13    #connected to Green LED
LED3_PIN    = 23    #connected to Blue LED
SWITCH_PIN  = 27    #connected to switch

#define a class for instantiating GPIO object
class mushroom (object):
    """Raspberry PI Internet 'IOT GPIO'."""

    #initial setup/constructor
    def __init__ (self):
        GPIO.setwarnings(False) #disable all warnings
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(LED1_PIN, GPIO.OUT)   #Red LED configured for output
        GPIO.setup(LED2_PIN, GPIO.OUT)   #Green LED configured for output
        GPIO.setup(LED3_PIN, GPIO.OUT)  #Blue LED configured for output
        GPIO.setup(SWITCH_PIN, GPIO.IN)
        GPIO.setup(SWITCH_PIN, GPIO.IN, pull_up_down= GPIO.PUD_UP)

    #method to read the value of the swith pin into a variable
    def read_switch(self):
        """Read the switch state."""
        switch = GPIO.input(SWITCH_PIN)
        #invert because of the active low momentary switch
        if (switch == 0):
            switch=1
        else:
            switch=0
        return switch

    def set_led(self, which_led, value):
        """Change the state of the LED based on the passed value. '1' for On and '0' for Off"""
        if (which_led == 1):
            GPIO.output(LED1_PIN, value)
        if (which_led == 2):
            GPIO.output(LED2_PIN, value)
        if (which_led == 3):
            GPIO.output(LED3_PIN, value)

    #getters and setters for LED pins
    def get_led(self, led):
        """Return the value of the LED state. '1' for On and '0' for Off"""
        if (led == 1):
            return GPIO.input(LED1_PIN)
        if (led == 2):
            return GPIO.input(LED2_PIN)
        if(led == 3):
            return GPIO.input(LED3_PIN)
