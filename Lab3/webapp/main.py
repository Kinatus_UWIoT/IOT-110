#this is the main interface file between the hardware driver gpio.py and the web server
import time
from flask import *
from gpio import mushroom
from debouncer import Debouncer

app = Flask(__name__)
iotcffb = mushroom()
db = Debouncer()
# ============================== API Routes ===================================
#all routes will go here
@app.route("/")
def index():
    return render_template('index.html') #return

# ============================ GET: /leds/<state> =============================
# read the LED status by GET method from curl for example
# curl http://iot8e3c:5000/leds/1
# curl http://iot8e3c:5000/leds/2
# -----------------------------------------------------------------------------
@app.route("/leds/<int:led_number>", methods=['GET'])
def leds(led_number):
    print ("LED ") + str(led_number) + " state:" + str(iotcffb.get_led(led_number))
    return "LED State: " + str(iotcffb.get_led(led_number)) + "\n"

# =============================== GET: /sw ====================================
# read the switch input by GET method from curl for example
# curl http://iot8e3c:5000/sw
# -----------------------------------------------------------------------------
@app.route("/sw", methods = ['GET'])
def sw():
    return "Switch state: " + str(iotcffb.read_switch()) + "!\n"

# ======================= POST: /ledcmd/<data> =========================
# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
# -----------------------------------------------------------------------------
@app.route("/ledcmd", methods = ['POST'])
def ledcommand():
    cmd_data = request.data
    print ("LED Command: ") + str(request.data) + ":\n"
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if (state == 'OFF'):
        iotcffb.set_led(led, False)
    elif (state == 'ON'):
        iotcffb.set_led(led, True)
    else:
        return "Argument Error"

    return "LED State Command:" + state + " for LED number " + str(led) + "!\n"
    # -----------------------------------------------------------------------------


# =========================== Endpoint: /myData ===============================
# read the gpio states by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_state_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            raw_switch = iotcffb.read_switch()
            debounced_switch = str(db.debounce(raw_switch))
            led_red = str(iotcffb.get_led(1))
            led_grn = str(iotcffb.get_led(2))
            led_blu = str(iotcffb.get_led(3))
            yield('data: {0}|{1}|{2}|{3}\n\n'.format(debounced_switch,led_red,led_grn,led_blu))
            time.sleep(0.1)
    return Response(get_state_values(), mimetype='text/event-stream')
# ============================== API Routes ===================================
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
