//<script>
  /* start executing only after the document has loaded */
  $(document).ready(function(){
  /* establish global variables for LED status */
      var led1;
      var led2;
      var led3;

  //    var iotSource = new EventSource("{{ url_for('myData') }}");
      /* intercept the incoming states from SSE */
      iotSource.onmessage = function(e) {
        console.log(e.data);
        var params = e.data.split('|');
        updateSwitch(params[0]);
        updateLeds(1,params[1]);
        updateLeds(2,params[2]);
        updateLeds(3,params[3]);
      }

      /* update the Switch based on its SSE state monitor */
      function updateSwitch(switchValue) {
        if (switchValue === '1') {
          $('#switch').toggleClass('label-default',false);
        } else if (switchValue === '0') {
          $('#switch').toggleClass('label-default',true);
        }
      }

      /* update the LEDs based on their SSE state monitor */
      function updateLeds(ledNum,ledValue) {
        if (ledNum === 1) {
          if (ledValue === '1') {
            //$('#red_led_label').toggleClass('label-default',false);
            $('#red_led_label').toggleClass('label-danger',true);
            led1 = "ON"
          } else if (ledValue === '0') {
          //  $('#red_led_label').toggleClass('label-default',true);
            $('#red_led_label').toggleClass('label-danger',false);
            led1 = "OFF"
          }
        }
        else if (ledNum === 2) {
          if (ledValue === '1') {
            $('#grn_led_label').toggleClass('label-success',true);
            //$('#grn_led_btn').toggleClass('button-success',true);
            led2 = "ON"
          } else if (ledValue === '0') {
            $('#grn_led_label').toggleClass('label-success',false);
            //$('#grn_led_btn').toggleClass('button-success',false);
            led2 = "OFF"
          }
        }
        else if (ledNum === 3) {
          if (ledValue === '1') {
            $('#blu_led_label').toggleClass('label-primary',true);
            //$('#blu_led_btn').toggleClass('button-primary',true);
            led3 = "ON"
          } else if (ledValue === '0') {
            $('#blu_led_label').toggleClass('label-primary',false);
            //$('#blu_led_btn').toggleClass('button-primary',false);
            led3 = "OFF"
          }
        }
      }


      //lets make sure our initial page load reads the data in sequence
      function initial_conditions(){
        var d = $.Deferred();

        setTimeout(function(){
          $.get('/leds/1', function(data){
            led1 = $.trim(data.split(':')[1]);
          });//$.get('/leds/1', function(data){

          $.get('/leds/2', function(data){
            led2 = $.trim(data.split(':')[1]);
          });//$.get('/leds/1', function(data){

          $.get('/leds/3', function(data){
            led3 = $.trim(data.split(':')[1]);
          });//$.get('/leds/1', function(data){

          console.log("Got my data", led1, led2, led3);
          d.resolve();
        },1);//setTimeout(function(){

        return d.done();
      }//function initial_condition(){

//lets initialite our LED vars to the current status of LED State "ON"/"OFF"
        function led_status(){
          var d = $.Deferred();

          setTimeout(function(){
            if (led1 === '0') {led1 = 'OFF'} else {led1 = 'ON'};
            if (led2 === '0') {led2 = 'OFF'} else {led2 = 'ON'};
            if (led3 === '0') {led3 = 'OFF'} else {led3 = 'ON'};
            d.resolve();
            console.log('Red:', led1);
            console.log('Green:', led2);
            console.log('Blue:', led3);
          }, 200);//          setTimeout(function(){
            return d.promise();
        }//        function led_status(){

initial_conditions().then(led_status);

      //button click function run asynchronously
      $('#red_led_btn').click(function() {
        if (led1 === "OFF"){led1 = "ON";} else {led1 = "OFF";}
        var params = 'led=1&state='+led1;
        console.log('LED Command with params:'+params);
        $.post('/ledcmd', params,function(data, status){
          console.log("Data: " + data + "\nStatus: "+ status);
        });//$post('/ledcmd', params,function(data, status)
      });//$(#red_led_btn).click(function(){

        $('#grn_led_btn').click(function(){
          if (led2 === "OFF"){led2 = "ON";} else {led2 = "OFF";}
          var params = 'led=2&state='+led2;
          console.log('Led Command with params:'+params);
          $.post('/ledcmd', params,function(data, status){
            console.log("Data: " + data + "\nStatus: "+ status);
          });//$post('/ledcmd', params,function(data, status)
        });//$(#rgrn_led_btn).click(function(){

          $('#blu_led_btn').click(function(){
            if (led3 === "OFF"){led3 = "ON";} else {led3 = "OFF";}
            var params = 'led=3&state='+led3;
            console.log('Led Command with params:'+params);
            $.post('/ledcmd', params,function(data, status){
              console.log("Data: " + data + "\nStatus: "+ status);
            });//$post('/ledcmd', params,function(data, status)
          });//$(#blu_led_btn).click(function(){
}) //$(document).ready(function()){
//</script>
