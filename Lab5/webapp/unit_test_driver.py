#!/usr/bin/python

import time

#link the driver library and import class
from gpio import mushroom

iotcffb = mushroom()    #call the constructor

#setup a loop for 50 rounds
print('Blinking LEDs for 50 cycles in sequence')

counter = 1
while (counter <= 50):
    print('\n ------ Counter: {0} ------'.format(counter))
    #Get the switch state and dislay on console
    switch = iotcffb.read_switch()
    print('\n============= Switch: {0} ============='.format(switch))

    print('\nLed 1 On (Red)')
    iotcffb.set_led(1, True)


    print('LED1: {0}'.format(iotcffb.get_led(1)))
    print('LED2: {0}'.format(iotcffb.get_led(2)))
    print('LED3: {0}'.format(iotcffb.get_led(3)))
    time.sleep(1)

    print('\nLed 2 On (Green)')
    iotcffb.set_led(2, True)
    print('LED1: {0}'.format(iotcffb.get_led(1)))
    print('LED2: {0}'.format(iotcffb.get_led(2)))
    print('LED3: {0}'.format(iotcffb.get_led(3)))
    time.sleep(1)

    print('\nLed 3 On (Blue)')
    iotcffb.set_led(3, True)
    print('LED1: {0}'.format(iotcffb.get_led(1)))
    print('LED2: {0}'.format(iotcffb.get_led(2)))
    print('LED3: {0}'.format(iotcffb.get_led(3)))
    time.sleep(1)

    print('\nLed 1 Off (Red)')
    iotcffb.set_led(1, False)
    print('LED1: {0}'.format(iotcffb.get_led(1)))
    print('LED2: {0}'.format(iotcffb.get_led(2)))
    print('LED3: {0}'.format(iotcffb.get_led(3)))
    time.sleep(1)

    print('\nLed 2 Off (Green)')
    iotcffb.set_led(2, False)
    print('LED1: {0}'.format(iotcffb.get_led(1)))
    print('LED2: {0}'.format(iotcffb.get_led(2)))
    print('LED3: {0}'.format(iotcffb.get_led(3)))
    time.sleep(1)

    print('\nLed 3 Off (Blue)')
    iotcffb.set_led(3, False)
    print('LED1: {0}'.format(iotcffb.get_led(1)))
    print('LED2: {0}'.format(iotcffb.get_led(2)))
    print('LED3: {0}'.format(iotcffb.get_led(3)))
    time.sleep(1)
    
    counter = counter + 1
