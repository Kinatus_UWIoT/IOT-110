#!/bin/bash
#short script to toggle a GPIO pin at the higest frequency possible
echo 23 > /sys/class/gpio/export
sleep 0.5
echo "out" > /sys/class/gpio/gpio23/direction
COUNTER=0
while [ $COUNTER -lt 100000 ]; do
  echo 1 > /sys/class/gpio/gpio23/value
  let COUNTER=COUNTER+1
  echo 0 > /sys/class/gpio/gpio23/value
done
echo 23 > /sys/class/gpio/unexport
